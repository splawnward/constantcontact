const constantcontact = require('./lib/api');

module.exports = ({version = 'v3', auth, rate_limit}) => constantcontact(version, auth, rate_limit);