const _ = require('lodash');

const getUrl = (url, params) =>  url.param(params, true).toString().replace(/\/:.*?(?=\/|$)/,'');

class Resource {

    constructor (url, methods, limiter) {
        this.url = url;
        this.limiter = limiter;

        for (let method of methods) {
            switch (method) {
                case 'GET':
                    this.find = async (params = {}, query = {}, paginate = false) => {
                        let response = await this.limiter.request({
                            method: method,
                            url: getUrl(this.url, params),
                            qs: query,
                            json: true
                        });

                        if (paginate && _.has(response, '_links.next')) {
                            let next = response['_links'].next.href;

                            do {
                                let next_response = await this.limiter.request({
                                    method: method,
                                    url: this.url.href.replace(/\/$/, '') + next,
                                    qs: query,
                                    json: true
                                });
                                _.mergeWith(response, next_response, (a, b) => {if (_.isArray(a)) return a.concat(b)});
                                next = _.get(next_response, '_links.next.href');
                            } while (next);

                            delete response['_links'];
                        }

                        return response
                    }
                    break;

                case 'PUT':
                    this.save = async (data, params, query = {}) => await this.limiter.request({
                        method: method,
                        url: getUrl(this.url, params),
                        qs: query,
                        json: data
                    })
                    break;

                case 'PATCH':
                    this.update = async (data, params, query = {}) => await this.limiter.request({
                        method: method,
                        url: getUrl(this.url, params),
                        qs: query,
                        json: data
                    })
                    break;

                case 'POST':
                    this.create = async (data, params = {}, query = {}) => await this.limiter.request({
                        method: method,
                        url: getUrl(this.url, params),
                        qs: query,
                        json: data
                    })
                    break;

                case 'DELETE':
                    this.delete = async (params, query = {}) => await this.limiter.request({
                        method: method,
                        url: getUrl(this.url, params),
                        qs: query,
                        json: true
                    })
                    break;
            }
        }
    }
}

module.exports = (limiter) => (url, methods) => new Resource(url, methods, limiter);