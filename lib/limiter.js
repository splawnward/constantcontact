const RequestRateLimiter = require('request-rate-limiter');
// import RequestRateLimiter, { BackoffError } from 'request-rate-limiter';

// class MyRequestHandler {
//     httpClient;
//
//     constructor (client) {
//         this.httpClient = client;
//     }
//
//     // this method is the only required interface to implement
//     // it gets passed the request config that is passed by the
//     // user to the request method of the limiter. The method must
//     // return an instance of the BackoffError when the limiter
//     // needs to back off
//     async request(requestConfig) {
//         try {
//             return await this.httpClient(requestConfig);
//         } catch (err) {
//             if (err.statusCode === 429) throw new BackoffError(err.message);
//             throw err;
//         }
//     }
// }

module.exports = (httpClient, options) => {
    const limiter = new RequestRateLimiter(options);

    function request(requestConfig) {
        return new Promise((resolve, reject) => {
            let retry = true;

            limiter.request((err, backoff) => {
                if (err) return reject(err);

                httpClient(requestConfig)
                    .then(resolve)
                    .catch(err => {
                        if (err.statusCode === 429 && err.error.error_key === 'throttled')
                            backoff();
                        else if (retry && err.statusCode >=500 && err.statusCode <= 599) {
                            retry = false;
                            backoff();
                        }
                        else
                            reject(err);
                    });
            });
        });

    }

    // limiter.setRequestHandler(new MyRequestHandler(httpClient));
    return { request };
}