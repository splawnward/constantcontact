const assert = require('assert');
const util = require('util');
const url_builder = require('url-assembler');
const request = require('request-promise-native');
const limiter = require('./limiter');

const sleep = util.promisify(setTimeout);

function recurse (property, endpoint, base_url, resourceFactory, client) {
    const url = base_url.segment(endpoint.url);
    let pointer = client;

    if (!endpoint.abstract)
        pointer = client[property] = resourceFactory(url, endpoint.methods);

    for (let key in endpoint.resources) {
        recurse(key, endpoint.resources[key], url, resourceFactory, pointer);
    }
}

module.exports = (version, auth, rate_limit = {}) => {
    // perform input validation
    assert.ok(version, 'version is required');
    assert.ok(auth, 'auth object is required');

    let { access_token, refresh_token, api_key, secret, notifier } = auth;
    assert.ok(access_token, 'auth.access_token is required');
    assert.ok(api_key, 'auth.api_key is required');

    // prepare http client with basic settings
    let httpOptions = {
        auth: {
            bearer: () => access_token
        }
    };
    let httpInterceptor;

    // wrap http client to automagically handle 401's with a refresh/retry
    if (version === 'v3' && refresh_token && secret) {
        let exchange_in_flight_prom = Promise.resolve();
        let exchange_in_flight_flag = false;

        const exchange_token = async () => {
            try {
                ({access_token, refresh_token} = await request.post({
                    url: 'https://idfed.constantcontact.com/as/token.oauth2',
                    auth: {
                        username: api_key,
                        password: secret
                    },
                    qs: {
                        grant_type: 'refresh_token',
                        refresh_token: refresh_token
                    },
                    json: true
                }));

                if (typeof notifier === 'function')
                    notifier(null, {access_token, refresh_token});
            }
            catch (err) {
                if (typeof notifier === 'function')
                    notifier(err);
            }

        }

        const unauthorized_interceptor = async (opts) => {
            try {
                // wait for any in-flight refreshes to prevent unnecessary 401's
                if (exchange_in_flight_flag)
                    await exchange_in_flight_prom;
                return await request(opts);
            }
            catch (err) {
                if (err.statusCode === 401) {
                    // this check prevents us from refreshing the token if it has already been refreshed
                    // while our request was being processed
                    if (err.response.request.headers.authorization === `Bearer ${access_token}`) {
                        // this check prevents multiple failed requests that might come back in the same tick
                        // from triggering multiple refreshes
                        if (exchange_in_flight_flag) {
                            await exchange_in_flight_prom;
                        }
                        else {
                            exchange_in_flight_flag = true;
                            exchange_in_flight_prom = exchange_token();
                            await exchange_in_flight_prom;
                            exchange_in_flight_flag = false;
                        }
                    }

                    return await request(opts);
                }

                throw err;
            }
        }

        httpInterceptor = unauthorized_interceptor;
    }

    let httpClient = request.defaults(httpOptions, httpInterceptor);

    const metadata = require(`./api.${version}.json`)
    const base_url = url_builder(metadata.base_url).prefix(metadata.prefix || '');

    let limiter_opts = Object.assign({}, {timeout: 300}, metadata.rate_limit, rate_limit);
    rate_limit.backoff = rate_limit.backoff || (rate_limit.requests)
    const rate_limiter = limiter(httpClient, {
        backoffTime: limiter_opts.backoff || (limiter_opts.interval / limiter_opts.requests + 1),
        rate: limiter_opts.requests,
        interval: limiter_opts.interval,
        maxWaitingTime: limiter_opts.timeout
    });

    const resource = require('./resource')(rate_limiter);

    const client = {
        waitFor: async (url, comparator, frequency = 30) => {
            do {
                await sleep(frequency * 1000);

                let data = await rate_limiter.request({
                    method: 'GET',
                    url: metadata.base_url + url,
                    json: true
                });

                if (comparator(data))
                    return data;
            } while (true);
        }
    }

    for (let key in metadata.resources) {
        if (!metadata.resources.hasOwnProperty(key))
            continue;

        recurse(key, metadata.resources[key], base_url, resource, client);
    }

    return client;
}